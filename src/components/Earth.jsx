import React, { useEffect, useRef } from "react";
import * as THREE from "three";

function ThreeAnimation() {
  const containerRef = useRef(null);

  useEffect(() => {
    // Set up the scene
    const scene = new THREE.Scene();

    // Set up the camera
    const camera = new THREE.PerspectiveCamera(
      75,
      containerRef.current.clientWidth / containerRef.current.clientHeight,
      0.1,
      1000
    );
    camera.position.z = 5;

    // Set up the renderer
    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(
      containerRef.current.clientWidth,
      containerRef.current.clientHeight
    );
    containerRef.current.appendChild(renderer.domElement);

    // Set up the earth mesh
    const geometry = new THREE.SphereGeometry(2, 32, 32);
    const texture = new THREE.TextureLoader().load(
      "https://cdn.pixabay.com/photo/2012/04/14/14/59/world-34219_960_720.png"
    );
    const material = new THREE.MeshBasicMaterial({ map: texture });
    const earthMesh = new THREE.Mesh(geometry, material);
    scene.add(earthMesh);

    // Animate the earth mesh
    const animate = () => {
      requestAnimationFrame(animate);
      earthMesh.rotation.y += 0.01;
      renderer.render(scene, camera);
    };
    animate();

    // Clean up
    return () => {
      containerRef.current.removeChild(renderer.domElement);
    };
  }, []);

  return <div className="w-full h-[400px]" ref={containerRef} />;
}

export default ThreeAnimation;
