import Typewriter from "typewriter-effect";
import { Yer, icon, vector, yer } from "./img";

function Header() {
  return (
    <div className="container pt-11 bg-[#1A1524] mx-auto flex  h-[915px] overflow-x-hidden">
      <div className="mx-[20px]  md:mx-[100px]">
        <h1 className="text-white w-[690px]  text-[48px] md:text-[72px] mt-14 md:mt-28">
          <span>
            <Typewriter
              onInit={(typewriter) => {
                typewriter
                  .typeString("All in one Fleet Management Solution & ELD")
                  .callFunction(() => {
                    console.log("String typed out!");
                  })
                  .start();
              }}
            />
          </span>
        </h1>
        <p className="text-white w-[550px] text-[16px] md:text-[18px] mt-6 md:mt-12">
          Dispatch Board will streamline day-to-day operations maximizing your
          time so you can focus on better loads and{" "}
          <a className="text-[#20DB81] pl-1" href="#">
            bigger profit.
          </a>
        </p>
        <form className="mt-6 md:mt-14">
          <div className="flex flex-col md:flex-row gap-2">
            <input
              type="text"
              placeholder="E-mail"
              className="w-full md:w-[400px] h-12 pl-6 bg-[#858487] rounded-[40px] text-white"
            />
            <button className="border rounded-[40px] w-full md:w-[200px] h-12 text-white bg-[#6D25E1] transition-all duration pb-1 flex gap-3 items-center px-6 hover:text-[#8D65FF]">
              Senden E-mail
              <img src={vector} alt="" />
            </button>
          </div>
          <button className="border rounded-[40px]  md:w-[230px] h-14 text-white bg-[#6D25E133] hover:transition-all duration pb-1 mt-4 md:mt-6 flex gap-3 justify-center items-center font-bold hover:text-[#8D65FF] md: w-[50px]">
            <img className="  md:hidden" src={icon} alt="" />
            <a className=" hidden md:block" href="#">Call us now</a>
            <img className=" hidden md:block" src={vector} alt="" />
          </button>
        </form>
      
      </div>
      <div className=" mr-[20px]  mt-6 md:block">
        <img
          className="w-full  h-[auto] animate-spin transition-all  rotate duration-100  "
          src={yer}
          alt=""
        />
      </div>
    </div>
  );
}

export default Header;
