import { Link } from "react-router-dom";
import { useState } from "react";
import { Logo, logo, vector } from "./img";


const NavBar = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div className=" container mx-auto max-w-screen-2xl px-[24px] py-3  md:px-24  sm:bg-[#1A1524] w-full">
      <div className=" flex items-center justify-between  ">
        <h1 className=" text-white">
          <img
            className="bg-cover w-[151px] md:w-[195px] cursor-pointer hidden sm:block"
            src={logo}
            alt=""
          />
          <img
            className="bg-cover w-[151px] md:w-[195px] cursor-pointer sm:hidden"
            src={Logo}
            alt=""
          />
        </h1>
        <ul className=" hidden md:flex gap-10 list-none">
          <li className=" hover:text-[#8D65FF] transition duration-75 text-white text-xl list-none ">
            <a href="#">Features</a>
          </li>
          <li className=" hover:text-[#8D65FF] transition duration-75 text-white text-xl">
            <a href="#">Devices</a>
          </li>
          <li className=" hover:text-[#8D65FF] transition duration-75 text-white text-xl">
            <a href="#">Pricing</a>
          </li>
          <li className=" hover:text-[#8D65FF] transition duration-75 text-white text-xl">
            <a href="#">Contact Us</a>
          </li>
        </ul>
        <button className="hidden border rounded-[40px] w-[148px] h-10 text-white bg-[#6D25E1;] hover:text-[#8D65FF] transition-all duration pb-1 md:flex items-center  justify-center gap-3">
          Sign in
          <img src={vector} alt="" />
        </button>
        {/* Hamburger menu */}
        <button
          type="button"
          className=" sm:hidden inline-flex items-center justify-center text-white focus:outline-none  focus:ring-inset focus:ring-white"
          aria-controls="mobile-menu"
          aria-expanded="false"
          onClick={() => setIsOpen(!isOpen)}
        >
          <button className={`${isOpen ? "hidden" : "block"} h-9 w-9`}>
            <div
              className="h-1 w-[28px] bg-black rounded"
              data-aos="fade-down"
              data-aos-duration="900"
            ></div>
            <div
              className="h-1 w-[28px] bg-black mt-[5px] rounded"
              data-aos="fade-down"
              data-aos-duration="1100"
            ></div>
            <div
              className="h-1 w-[28px] bg-black mt-[5px] rounded"
              data-aos="fade-down"
              data-aos-duration="1300"
            ></div>
          </button>
          <svg
            className={`${isOpen ? "block" : "hidden"} h-9 w-9 text-black`}
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            aria-hidden="true"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        </button>
      </div>
      {/* Mobile menu */}
      <div className=" mt-[12px] absolute bg-[#ffffff]  w-full left-0">
        <div
          className={`${isOpen ? "block" : "hidden"} sm:hidden`}
          id="mobile-menu"
        >
          <div className="px-2 pt-2 pb-3 space-y-1">
            <li className=" text-[19px] uppercase my-2 mx-[1px] px-4 py-2 text-black hover:bg-slate-600 rounded font-Monserat font-medium list-none">
              About us
            </li>
            <div class="border-dashed border-2 border-[#000000] mx-[20px] opacity-10 "></div>
            <li className=" text-[19px] uppercase my-2 mx-[1px] px-4 py-2 text-black hover:bg-slate-600 rounded font-Monserat font-medium  list-none">
              Features
            </li>
            <div class="border-dashed border-2 border-[#000000] mx-[20px] opacity-10 "></div>
            <li className=" text-[19px] uppercase my-2 mx-[1px] px-4 py-2 text-black hover:bg-slate-600 rounded font-Monserat font-medium  list-none">
              Devices
            </li>
            <div class="border-dashed border-2 border-[#000000] mx-[20px] opacity-10 "></div>
         
            <li className=" text-[19px] uppercase my-2 mx-[1px] px-4 py-2 text-black hover:bg-slate-600 rounded font-Monserat font-medium  list-none">
              Pricing
            </li>
            <div class="border-dashed border-2 border-[#000000] mx-[20px] opacity-10 "></div>
            <li className=" text-[19px] uppercase my-2 mx-[1px] px-4 py-2 text-black hover:bg-slate-600 rounded font-Monserat font-medium  list-none">
              Contact Us
            </li>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
