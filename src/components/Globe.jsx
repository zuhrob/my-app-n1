import React, { useEffect, useRef } from "react";
import * as THREE from "three";
import { Yer } from "./img";

function Globe() {
  const canvasRef = useRef();

  useEffect(() => {
    const canvas = canvasRef.current;

    const scene = new THREE.Scene();

    const camera = new THREE.PerspectiveCamera(
      75,
      canvas.clientWidth / canvas.clientHeight,
      0.1,
      1000
    );

    const renderer = new THREE.WebGLRenderer({ canvas });

    renderer.setSize(canvas.clientWidth, canvas.clientHeight);

    const sphereGeometry = new THREE.SphereGeometry(1, 32, 32);

    const textureLoader = new THREE.TextureLoader();

    const globeTexture = textureLoader.load(
      "https://threejs.org/examples/textures/land_ocean_ice_cloud_2048.jpg"
    );

    const globeMaterial = new THREE.MeshBasicMaterial({ map: globeTexture });

    const globeMesh = new THREE.Mesh(sphereGeometry, globeMaterial);

    scene.add(globeMesh);

    camera.position.z = 2;

    const animate = () => {
      requestAnimationFrame(animate);

      globeMesh.rotation.y += 0.005;

      renderer.render(scene, camera);
    };

    animate();
  }, []);

  return <canvas ref={canvasRef} />;
}
export default Globe;
